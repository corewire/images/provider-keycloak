
#!/bin/bash
set -euox pipefail

########################
####### Versions #######
########################

# If CI_COMMIT_TAG is set, use this tag as VERSION, otherwise use the default.
if [[ ${CI_COMMIT_TAG+x} ]]; then
  VERSION="$CI_COMMIT_TAG"
  export VERSION
  # Print the value of VERSION for verification
  echo "VERSION=$VERSION"
else
  echo "CI_COMMIT_TAG is not set. VERSION variable not exported."
fi

# Terraform version
# renovate: datasource=github-releases depName=hashicorp/terraform
export TERRAFORM_VERSION="1.4.6"

# Terraform provider version
# renovate: datasource=github-releases depName=mrparkers/terraform-provider-keycloak
export TERRAFORM_PROVIDER_VERSION="4.2.0"

########################
###### Variables #######
########################

export PROVIDER_NAME_LOWER=keycloak
export PROVIDER_NAME_NORMAL=keycloak
export ORGANIZATION_NAME=corewire
export PROVIDER_DIR="src/provider-${PROVIDER_NAME_LOWER}"
export CONFIG_DIR="configuration"
export PROJECT_NAME=provider-keycloak
export DISABLE_GITHUB=false
export ENABLE_CUSTOM_API_NAME=true
export CUSTOM_API_NAME="${PROVIDER_NAME_LOWER}.crossplane.io"

# Repository name
export PROJECT_REPO=github.com/corewire/${PROJECT_NAME}

# Terraform provider source
export TERRAFORM_PROVIDER_SOURCE=mrparkers/keycloak

# Terraform provider repository that contains the source code/release,
export TERRAFORM_PROVIDER_REPO=https://github.com/mrparkers/terraform-provider-keycloak

# Provider download name is the name of the release
# e.g. https://github.com/mrparkers/terraform-provider-keycloak/releases contains terraform-provider-keycloak_4.2.0_linux_amd64.zip
export TERRAFORM_PROVIDER_DOWNLOAD_NAME=terraform-provider-keycloak
export TERRAFORM_PROVIDER_DOWNLOAD_URL_PREFIX=${TERRAFORM_PROVIDER_REPO}/releases/download/v${TERRAFORM_PROVIDER_VERSION}

# Binary name of the provider, usually providers are packaged as zips, which contain a binary.
export TERRAFORM_NATIVE_PROVIDER_BINARY=terraform-provider-keycloak_v${TERRAFORM_PROVIDER_VERSION}

# Documentation path.
export TERRAFORM_DOCS_PATH=docs/resources

# Platforms to build the provider for, e.g. linux_amd64, darwin_amd64, windows_amd64 - basically everything that is supported by buildx.
# Should be pretty much always linux_amd64 unless you want to build for ARM or something and run on mac servers.
export PLATFORMS=${PLATFORMS:=linux_amd64 linux_arm64}

# Target Registry to push the provider to.
export REGISTRY_ORGS=${CI_REGISTRY}/corewire/images
export XPKG_REG_ORGS=${REGISTRY_ORGS}
export XPKG_REG_ORGS_NO_PROMOTE=${REGISTRY_ORGS}

echo "PROVIDER_NAME_LOWER: $PROVIDER_NAME_LOWER"
echo "PROVIDER_NAME_NORMAL: $PROVIDER_NAME_NORMAL"
echo "ORGANIZATION_NAME: $ORGANIZATION_NAME"

########################
###### Pre Build #######
########################

mkdir src || true
rm -R -f ${PROVIDER_DIR}
git clone https://github.com/corewire/provider-keycloak ${PROVIDER_DIR}

pushd "${PROVIDER_DIR}"

# Dummy user for CI builds to avoid dirty states
git config user.email "you@example.com"
git config user.name "Your Name"

########################
######## Build #########
########################

# Initialize submodules
make submodules
# Generate code and CRDs
make generate

# Dummy commit to avoid dirty git state
git status
git add -A
git commit -m "dummy commit" --allow-empty

# Build the provider and push it to the registry
make build.all
make publish
